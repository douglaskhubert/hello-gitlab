var express = require('express');

app = express();
port = process.env.PORT || 3000;
mongoose = require('mongoose');
Message = require('./api/models/msgModel');
bodyParser = require('body-parser');

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://172.17.0.2/msgdb');
mongoose.connect('mongodb://' + process.env.DB_USER + ':' + process.env.DB_PASSWORD + '@ds157223.mlab.com:57223/hello-gitlab')
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var routes = require('./api/routes/msgRoutes');
routes(app);

app.listen(port);

console.log('Message RESTful API server started on: ' + port);
